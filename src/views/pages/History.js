import React, { Component } from 'react';
import { Card, CardBody, Badge,UncontrolledDropdown,DropdownToggle,DropdownMenu,DropdownItem,Button} from 'reactstrap';
import DataTable from 'react-data-table-component';
import { ChevronDown, Check } from 'react-feather';
import Checkbox from '../../components/@vuexy/checkbox/CheckboxesVuexy';
import "../../assets/scss/plugins/extensions/react-paginate.scss"
import "../../assets/scss/pages/data-list.scss"
import { connect } from 'react-redux';
import { appointment } from '../../redux/actions/orderList';
const CustomHeader = props => {
  return (
    <div className="data-list-header d-flex justify-content-between flex-wrap">
      <div className="actions-left d-flex flex-wrap">
       <h3>Appointments</h3>
        {/* <Button
          className="add-new-btn"
          color="primary"
          onClick={() => props.handleSidebar(true, true)}
          outline>
          <Plus size={15} />
          <span className="align-middle">Add New</span>
        </Button> */}
      </div>
    </div>
  )
}

class History extends Component {
  state = {
    data: [
      {
        
        image: require('../../assets/img/portrait/small/avatar-s-2.jpg'),
        name: 'Alyss Lillecrop',
        email: 'alillecrop0@twitpic.com',
        medicine:"Aceta",
        invoice:"#1",
        status: 'active',
        revenue: '$32,000',
      },
      {
        image: require('../../assets/img/portrait/small/avatar-s-3.jpg'),
        name: 'Gasper Morley',
        email: 'gmorley2@chronoengine.com',
        medicine:"Aceta",
        invoice:"#2",
        status: 'active',
        revenue: '$78,000',
      },
      {
        image: require('../../assets/img/portrait/small/avatar-s-4.jpg'),
        name: 'Phaedra Jerrard',
        email: 'pjerrard3@blogs.com',
        medicine:"Aceta",
        invoice:"#3",
        status: 'inactive',
        revenue: '$10,000',
      },
    ],
    totalPages: 0,
    currentPage: 0,
    columns: [
      {
        name: 'Patient Name',
        selector: 'name',
        sortable: false,
        minWidth: '200px',
        cell: (row) => (
          <div className="d-flex  flex-column align-items-start py-xl-0 py-1">
            <div className="user-img ml-xl-0 ml-2">
              <img
                className="img-fluid rounded-circle"
                height="36"
                width="36"
                src={row.image}
                alt={row.name}
              />
            </div>
            <br />
            <div className="user-info text-truncate ml-0">
              <p
                title={row.name}
                className="d-block text-bold-600 text-truncate mb-0"
              >
                {row.name}
              </p>
            </div>
          </div>
        ),
      },
      {
        name: 'Disease',
        selector: 'disease',
        sortable: false,
        cell: (row) => (
          <p className="text-bold-600 text-truncate mb-0">{row.disease}</p>
        ),
      },
      {
        name: 'Test',
        selector: 'test',
        sortable: false,
        cell: (row) => (
          <p className="text-bold-600 text-truncate mb-0">{row.test}</p>
        ),
      },
      {
        name: 'Date',
        selector: 'date',
        sortable: false,
        cell: (row) => <p className="text-bold-600 mb-0">{row.date}</p>,
      },
      {
        name: 'Time',
        selector: 'time',
        sortable: false,
        cell: (row) => <p className="text-bold-600 mb-0">{row.time}</p>,
      },
      {
        name: 'Status',
        selector: 'appointment_status',
        sortable: false,
        cell: (row) => (
          <Badge
            outline
            className="border-primary"
            style={{backgroundColor:"#f1f1f3",fontWeight:"600",fontSize:"12px"}}
            color={row.status === 'inactive' ? 'light-danger' : 'light-primary'}
          >
            {row.appointment_status}
          </Badge>
        ),
      },
     
    ],
    allData: [],
    value: '',
    rowsPerPage: 4,
    sidebar: false,
    currentData: null,
    selected: [],
    totalRecords: 0,
    sortIndex: [],
    addNew: '',
  };
  componentDidMount(){
    this.props.appointment()
  }
  render() {
    let { columns, data } = this.state;
    return (
      <Card>
        <CardBody>
        <div
        className="data-list">
          <DataTable
            columns={columns}
            data={this.props.completed_order}
            pagination
            paginationServer
            //   paginationComponent={() => (
            //     <ReactPaginate
            //       previousLabel={<ChevronLeft size={15} />}
            //       nextLabel={<ChevronRight size={15} />}
            //       breakLabel="..."
            //       breakClassName="break-me"
            //       pageCount={totalPages}
            //       containerClassName="vx-pagination separated-pagination pagination-end pagination-sm mb-0 mt-2"
            //       activeClassName="active"
            //       forcePage={
            //         this.props.parsedFilter.page
            //           ? parseInt(this.props.parsedFilter.page - 1)
            //           : 0
            //       }
            //       onPageChange={page => this.handlePagination(page)}
            //     />
            //   )}
            pagination={false}
            noHeader
            subHeader
            //selectableRows
            responsive
            pointerOnHover
            selectableRowsHighlight
            //   onSelectedRowsChange={data =>
            //     this.setState({ selected: data.selectedRows })
            //   }
            //customStyles={selectedStyle}
            subHeaderComponent={
              <CustomHeader
                // handleSidebar={this.handleSidebar}
                // handleFilter={this.handleFilter}
                // handleRowsPerPage={this.handleRowsPerPage}
                // rowsPerPage={rowsPerPage}
                // total={totalRecords}
                // index={sortIndex}
              />
            }
            sortIcon={<ChevronDown />}
            //selectableRowsComponent={Checkbox}
            // selectableRowsComponentProps={{
            //   color: 'primary',
            //   icon: <Check className="vx-icon" size={12} />,
            //   label: '',
            //   size: 'sm',
            // }}
          />
          </div>
        </CardBody>
      </Card>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    loading: state.auth,
    completed_order:state.orderList.appointments
  };
};

export default connect(mapStateToProps, {
  appointment
})(History);
