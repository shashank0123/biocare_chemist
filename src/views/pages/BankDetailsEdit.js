import React, { Component } from 'react';
import {
  Card,
  CardBody,
  CardHeader,
  Media,
  CardTitle,
  CustomInput,
  Row,
  Col,
  FormGroup,
  Label,
  Input,
} from 'reactstrap';
import '../../assets/scss/pages/users.scss';
import userImg from '../../assets/img/portrait/small/avatar-s-18.jpg';
import { Edit3 } from 'react-feather';
import { connect } from 'react-redux';
import { getBankDetails, updateBankDetails } from '../../redux/actions/banks';
import { Link } from 'react-router-dom';

class BankDetails extends Component {
  state = {
    bank_name: '',
    account_holder_name: '',
    ifsc: '',
    gstin: '',
    branch_name: '',
    account_number: '',
    pancard: '',
    getValue: '',
  };
  static getDerivedStateFromProps(props, state) {
    if (props.bank_details.getValue != state.getValue) {
      console.log('Props', props, 'states', state);
      return {
        bank_name: props.bank_details.bank_name,
        account_holder_name: props.bank_details.account_holder_name,
        ifsc: props.bank_details.ifsc,
        gstin: props.bank_details.gstin,
        branch_name: props.bank_details.branch_name,
        account_number: props.bank_details.account_number,
        pancard: props.bank_details.pancard,
        getValue: props.getValue,
      };
    }

    return null;
  }
  handleSubmit() {
    const value = {
      bank_name: this.state.bank_name,
      account_holder_name: this.state.account_holder_name,
      ifsc: this.state.ifsc,
      gstin: this.state.gstin,
      branch_name: this.state.bank_name,
      account_number: this.state.account_number,
      pancard: this.state.pancard,
    };
    this.props.updateBankDetails(value);
  }
  componentDidMount() {
    this.props.getBankDetails();
  }
  render() {
    const {
      bank_name,
      account_holder_name,
      ifsc,
      gstin,
      branch_name,
      account_number,
      pancard,
      BankDetails,
    } = this.state;
    return (
      <Card>
        <CardHeader>
          <div className="title">
            <CardTitle>General Informations</CardTitle>
          </div>
        </CardHeader>
        <CardBody>
          <Row>
            <Col lg="6" md="6" sm="12">
              <FormGroup>
                <Label for="basicInput">Account Holder Name</Label>
                <Input
                  type="text"
                  id="basicInput"
                  value={account_holder_name}
                  onChange={(e) => {
                    this.setState({
                      account_holder_name: e.target.value,
                    });
                  }}
                />
              </FormGroup>
            </Col>
            <Col lg="6" md="6" sm="12">
              <FormGroup>
                <Label for="basicInput">Bank Name</Label>
                <Input
                  type="text"
                  id="basicInput"
                  value={bank_name}
                  onChange={(e) => {
                    this.setState({
                      bank_name: e.target.value,
                    });
                  }}
                />
              </FormGroup>
            </Col>
            <Col lg="6" md="6" sm="12">
              <FormGroup>
                <Label for="basicInput">Branch</Label>
                <Input
                  type="text"
                  id="basicInput"
                  value={branch_name}
                  onChange={(e) => {
                    this.setState({
                      branch_name: e.target.value,
                    });
                  }}
                />
              </FormGroup>
            </Col>
            <Col lg="6" md="6" sm="12">
              <FormGroup>
                <Label for="basicInput">Account Number</Label>
                <Input
                  type="text"
                  id="basicInput"
                  value={account_number}
                  onChange={(e) => {
                    this.setState({
                      account_number: e.target.value,
                    });
                  }}
                />
              </FormGroup>
            </Col>
            <Col lg="6" md="6" sm="12">
              <FormGroup>
                <Label for="basicInput">IFSC Code</Label>
                <Input
                  type="text"
                  id="basicInput"
                  value={ifsc}
                  onChange={(e) => {
                    this.setState({
                      ifsc: e.target.value,
                    });
                  }}
                />
              </FormGroup>
            </Col>
            <Col lg="6" md="6" sm="12">
              <FormGroup>
                <Label for="basicInput">PAN No.</Label>
                <Input
                  type="text"
                  id="basicInput"
                  value={pancard}
                  onChange={(e) => {
                    this.setState({
                      pancard: e.target.value,
                    });
                  }}
                />
              </FormGroup>
            </Col>
            <Col lg="6" md="6" sm="12">
              <FormGroup>
                <Label for="basicInput">GSTIN</Label>
                <Input
                  type="text"
                  id="basicInput"
                  value={gstin}
                  onChange={(e) => {
                    this.setState({
                      gstin: e.target.value,
                    });
                  }}
                />
              </FormGroup>
            </Col>
            <Col lg="12" md="12" sm="12" className="account-footer">
              <FormGroup>
                <div className="button-footer">
                  <button
                    className="btn reset-all"
                    onClick={() => {
                      console.log('here');
                    }}
                    style={{ fontWeight: '600' }}
                  >
                    Reset All
                  </button>
                  <button
                    className="btn btn-primary float-right "
                    onClick={() => this.handleSubmit()}
                    style={{ fontWeight: '600' }}
                  >
                    Save Changes
                  </button>
                </div>
              </FormGroup>
            </Col>
          </Row>
        </CardBody>
      </Card>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    bank_details: state.bank.bank_details,
  };
};

export default connect(mapStateToProps, {
  getBankDetails,
  updateBankDetails,
})(BankDetails);
