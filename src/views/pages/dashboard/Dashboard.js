import React from 'react';
import DataList from './DataList';
import Graphs from './Graphs';
import { connect } from 'react-redux';
import { getDashboard } from '../../../redux/actions/dashboard';

class Home extends React.Component {
  componentDidMount(){
    this.props.getDashboard()
  }
  render() {
    return (
      <React.Fragment>
        <Graphs />
        <DataList />
      </React.Fragment>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    loading: state.auth,
    completed_order:state.orderList.completed_order
  };
};

export default connect(mapStateToProps, {
  getDashboard,
})(Home);
