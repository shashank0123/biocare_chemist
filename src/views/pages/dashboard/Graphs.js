import React, { Component } from 'react';
import { Card, Row, Col, CardBody, Progress, CardHeader } from 'reactstrap';
import Chart from 'react-apexcharts';
import {
  ShoppingCart,
  ArrowUp,
  ArrowDown,
  CreditCard,
  Tag,
  ShoppingBag,
} from 'react-feather';
import StatisticsCard from '../../../components/@vuexy/statisticsCard/StatisticsCard';
import { subscribersGained, subscribersGainedSeries } from './StatisticsData';
import '../../../assets/scss/custom.css';
import { connect } from 'react-redux';

class Graphs extends Component {
  state = {
    options: {
      chart: {
        toolbar: {
          show: false,
        },

        dropShadow: {
          enabled: true,
          enabledOnSeries: [0, 1],
          top: 15,
          right: 11,
          blur: 4,
          color: ['#E7515A', '#1B55E2'],
          opacity: 0.45,
        },
        animations: {
          enabled: false,
        },
      },

      stroke: {
        curve: 'smooth',
        //dashArray: [0, 8],
        width: [3, 3],
      },
      grid: {
        borderColor: '#e7eef7',
      },
      legend: {
        show: true,
        position: 'top',
        horizontalAlign: 'right',
      },
      colors: ['#E7515A', '#1B55E2'],

      // fill: {
      //   type: 'gradient',
      //   gradient: {
      //     shade: 'dark',
      //     inverseColors: false,
      //     gradientToColors: ['#9c8cfc', '#b9c3cd'],
      //     shadeIntensity: 1,
      //     type: 'horizontal',
      //     opacityFrom: 1,
      //     opacityTo: 1,
      //     stops: [0, 100, 100, 100],
      //   },
      // },
      markers: {
        size: 0,
        hover: {
          size: 5,
        },
      },
      xaxis: {
        labels: {
          style: {
            colors: '#b9c3cd',
          },
        },
        axisTicks: {
          show: false,
        },
        categories: [
          'Jan',
          'Feb',
          'Mar',
          'Apr',
          'May',
          'Jun',
          'Jul',
          'Aug',
          'Sep',
          'Oct',
          'Nov',
          'Dec',
        ],
        axisBorder: {
          show: false,
        },
        tickPlacement: 'on',
      },
      yaxis: {
        tickAmount: 2,
        labels: {
          style: {
            color: '#b9c3cd',
          },
          formatter: (val) => {
            return val > 999 ? (val / 1000).toFixed(1) + 'k' : val;
          },
        },
      },
      tooltip: {
        x: { show: false },
      },
    },
    series: [
      {
        name: 'Expenses',
        data: [],
      },
      {
        name: 'Income',
        data: [],
      },
    ],
    total_order: [
      {
        name: 'Sales',
        data: [1, 2],
      },
    ],
  };
  static getDerivedStateFromProps(props, state) {
    console.log(
      'Props',
      props.total_order_sales.length,
      'states',
      state.total_order[0].data.length
    );
    if (props.total_order_sales.length !== state.total_order[0].data.length&&props.revenue_expenses.length !== state.series[0].data.length ) {
      return {
        series: [
          {
            name: 'Expenses',
            data: props.revenue_expenses,
          },
          {
            name: 'Income',
            data: props.revenue_income,
          },
        ],
        total_order: [
          {
            name: 'Sales',
            data: props.total_order_sales,
          },
        ],
      };
    }

    
    return null;
  }
  render() {
    return (
      <div>
        <Row>
          <Col lg="12" md="12" sm="12">
            <Card>
              <CardBody>
                <div className="data-list-header d-flex justify-content-between flex-wrap mb-1">
                  <div className="actions-left d-flex flex-wrap">
                    <h3>Revenue</h3>
                    {/* <Button
          className="add-new-btn"
          color="primary"
          onClick={() => props.handleSidebar(true, true)}
          outline>
          <Plus size={15} />
          <span className="align-middle">Add New</span>
        </Button> */}
                  </div>
                </div>
                <div className="d-flex justify-content-start mb-1">
                  <div>
                    <h2 className="text-bold-400">
                      <sup className="font-medium-1 mr-50">$</sup>
                      <span>{this.props.revenue.total_profit}</span>
                    </h2>
                    <p className="mb-50 text-bold-600 position-absolute">
                      Total Profit
                    </p>
                  </div>
                </div>
                <Chart
                  options={this.state.options}
                  series={this.state.series}
                  type="line"
                  height={260}
                />
              </CardBody>
            </Card>
          </Col>

          <Col lg="4" md="4" sm="12">
            <Card>
              <CardBody></CardBody>
            </Card>
          </Col>
          <Col lg="4" md="4" sm="12">
            <Card>
              <CardHeader>
                <h4>Summary</h4>
              </CardHeader>
              <CardBody>
                <div className="d-flex justify-content-between mb-25">
                  <div className="icon-section mr-1">
                    <div className="avatar avatar-stats m-0 bg-income">
                      <div className="avatar-content">
                        <ShoppingBag size="16" />
                      </div>
                    </div>
                  </div>
                  <div className="flex-grow-1">
                    <p className="float-left m-0 text-bold-600">Income</p>
                    <p className="float-right m-0 text-bold-600">
                      ${this.props.summary.income}
                    </p>
                    <Progress
                      barClassName="progress-bar-income"
                      className="mb-2 w-100 "
                      value="73"
                    />
                  </div>
                </div>
                <div className="d-flex justify-content-between mb-25">
                  <div className="icon-section mr-1">
                    <div className="avatar avatar-stats m-0 bg-profit">
                      <div className="avatar-content">
                        <Tag size="16" />
                      </div>
                    </div>
                  </div>
                  <div className="flex-grow-1">
                    <p className="float-left m-0 text-bold-600">Profit</p>
                    <p className="float-right m-0 text-bold-600">
                      ${this.props.summary.profile}
                    </p>
                    <Progress
                      barClassName="progress-bar-profit"
                      className="mb-2 w-100"
                      value="73"
                    />
                  </div>
                </div>
                <div className="d-flex justify-content-between mb-25">
                  <div className="icon-section mr-1">
                    <div className="avatar avatar-stats m-0 bg-expenses">
                      <div className="avatar-content">
                        <CreditCard size="16" />
                      </div>
                    </div>
                  </div>
                  <div className="flex-grow-1">
                    <p className="float-left m-0 text-bold-600">Expenses</p>
                    <p className="float-right m-0 text-bold-600">
                      ${this.props.summary.expenses}
                    </p>
                    <Progress
                      barClassName="progress-bar-expenses"
                      className="mb-2 w-100"
                      value="73"
                    />
                  </div>
                </div>
              </CardBody>
            </Card>
          </Col>
          <Col lg="4" md="4" sm="12">
            <StatisticsCard
              icon={<ShoppingCart className="white" size={22} />}
              stat={this.props.total_order.total_order}
              statTitle="Total Orders"
              options={subscribersGained}
              series={this.state.total_order}
              type="area"
            />
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    revenue: state.dashboard.revenue,
    summary: state.dashboard.summary,
    total_order: state.dashboard.total_order,
    total_order_sales: state.dashboard.total_order_sales,
    revenue_expenses: state.dashboard.revenue_expenses,
    revenue_income: state.dashboard.revenue_income,
    revenue_months: state.dashboard.revenue_months,
  };
};

export default connect(mapStateToProps, {})(Graphs);
