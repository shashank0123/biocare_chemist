import React, { Component } from 'react';
import {
  Card,
  CardBody,
  CardHeader,
  Media,
  CardTitle,
  CustomInput,
} from 'reactstrap';
import '../../assets/scss/pages/users.scss';
import userImg from '../../assets/img/portrait/small/avatar-s-18.jpg';
import { Edit3 } from 'react-feather';
import { connect } from 'react-redux';
import { getBankDetails } from '../../redux/actions/banks';
import { Link } from 'react-router-dom';

class BankDetails extends Component {
  state={
    bank_name:'',
    account_holder_name:'',
    ifsc:'',
    gstin:'',
    branch_name:'',
    account_number:'',
    pancard:''
  }
  static getDerivedStateFromProps(props, state) {
    console.log('Props', props, 'states', state);
    if (true) {
      return {
        bank_name:props.bank_details.bank_name,
        account_holder_name:props.bank_details.account_holder_name,
        ifsc:props.bank_details.ifsc,
        gstin:props.bank_details.gstin,
        branch_name:props.bank_details.branch_name,
        account_number:props.bank_details.account_number,
        pancard:props.bank_details.pancard
      };
    }

    return null;
  }

  componentDidMount() {
    this.props.getBankDetails();
  }
  render() {
   const {bank_name,account_holder_name,ifsc,gstin,branch_name,account_number,pancard,BankDetails}=this.state
    return (
      <Card>
        <CardHeader>
          <div className="title">
            <CardTitle>Bank Info</CardTitle>
          </div>
          <Link to="bank-details/edit" className="edit-profile">
            <Edit3 className="cursor-pointer" size={20} />
          </Link>
        </CardHeader>
        <CardBody>
          <div className="users-page-view-table">
            <div className="d-flex user-info">
              <div className="user-info-title font-weight-bold">Bank Name:</div>
              <div>{bank_name}</div>
            </div>
            <div className="d-flex user-info">
              <div className="user-info-title font-weight-bold">
                Account Holder Name:
              </div>
              <div>{account_holder_name}</div>
            </div>
            <div className="d-flex user-info">
              <div className="user-info-title font-weight-bold">IFSC:</div>
              <div>{ifsc}</div>
            </div>
            <div className="d-flex user-info">
              <div className="user-info-title font-weight-bold">GSTIN:</div>
              <div>{gstin}</div>
            </div>
            <div className="d-flex user-info">
              <div className="user-info-title font-weight-bold">
                Branch Name:
              </div>
              <div>{branch_name}</div>
            </div>
            <div className="d-flex user-info">
              <div className="user-info-title font-weight-bold">
                Account No:
              </div>
              <div>{account_number}</div>
            </div>
            <div className="d-flex user-info">
              <div className="user-info-title font-weight-bold">PAN:</div>
              <div>{pancard}</div>
            </div>
          </div>
        </CardBody>
      </Card>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    bank_details: state.bank.bank_details,
  };
};

export default connect(mapStateToProps, {
  getBankDetails,
})(BankDetails);
