import React, { Component,useState, useEffect  } from 'react';
import {
  Card,
  CardBody,
  CardHeader,
  Media,
  CardTitle,
  CustomInput,
  Row,
  Col,
  FormGroup,
  Label,
  Input,
} from 'reactstrap';
import '../../assets/scss/pages/users.scss';
import userImg from '../../assets/img/portrait/small/avatar-s-18.jpg';
import { Edit3 } from 'react-feather';
import { connect } from 'react-redux';
import { updateProfile } from '../../redux/actions/auth/loginActions';
import { Link } from 'react-router-dom';
import { useDropzone } from "react-dropzone"
import "../../assets/scss/plugins/extensions/dropzone.scss"



function BasicDropzone(props) {
  const [files, setFiles] = useState([])
  const { getRootProps, getInputProps } = useDropzone({
    accept: "image/*",
    onDrop: acceptedFiles => {
      setFiles(
        acceptedFiles.map(file =>
          Object.assign(file, {
            preview: URL.createObjectURL(file)
          })
        )
      )
    }
  })

  const thumbs = files.map(file => (
    <div className="dz-thumb" key={file.name}>
      <div className="dz-thumb-inner">
        <img src={file.preview} className="dz-img" alt={file.name} />
      </div>
    </div>
  ))

  useEffect(
    () => () => {
      // Make sure to revoke the data uris to avoid memory leaks
      files.forEach(file => URL.revokeObjectURL(file.preview))
    },
    [files]
  )

  return (
    <section>
      <div {...getRootProps({ className: "dropzone" })}>
        <input {...getInputProps()} />
        <p className="mx-1">
         
        </p>
      </div>
      <aside className="thumb-container">{thumbs}</aside>
    </section>
  )
}

class ProfileEdit extends Component {
  state = {
    bank_name: '',
    account_holder_name: '',
    ifsc: '',
    gstin: '',
    branch_name: '',
    account_number: '',
    pancard: '',
    getValue: '',
  };
  static getDerivedStateFromProps(props, state) {
    if (props.bank_details.getValue != state.getValue) {
      console.log('Props', props, 'states', state);
      return {
        // bank_name: props.bank_details.bank_name,
        // account_holder_name: props.bank_details.account_holder_name,
        // ifsc: props.bank_details.ifsc,
        // gstin: props.bank_details.gstin,
        // branch_name: props.bank_details.branch_name,
        // account_number: props.bank_details.account_number,
        // pancard: props.bank_details.pancard,
        // getValue: props.getValue,
      };
    }

    return null;
  }
  handleSubmit() {
    const value = {
      bank_name: this.state.bank_name,
      account_holder_name: this.state.account_holder_name,
      ifsc: this.state.ifsc,
      gstin: this.state.gstin,
      branch_name: this.state.bank_name,
      account_number: this.state.account_number,
      pancard: this.state.pancard,
    };
    this.props.updateBankDetails(value);
  }
  componentDidMount() {
    this.props.updateProfile();
  }
  render() {
    const {
      bank_name,
      account_holder_name,
      ifsc,
      gstin,
      branch_name,
      account_number,
      pancard,
      BankDetails,
    } = this.state;
    return (
      <React.Fragment>
           <Card>
        <CardHeader>
          <div className="title">
            <CardTitle>General Informations</CardTitle>
          </div>
        </CardHeader>
        <CardBody>
          <Row>
            <Col lg="2" md="2">
            <BasicDropzone />
            <h6 className="text-center">Upload Picture</h6>
            </Col>
            <Col lg="10" md="10">
              <Row>
              <Col lg="6" md="6" sm="12">
              <FormGroup>
                <Label for="basicInput">Pharmacy Name</Label>
                <Input
                  type="text"
                  id="basicInput"
                  value={account_holder_name}
                  onChange={(e) => {
                    this.setState({
                      account_holder_name: e.target.value,
                    });
                  }}
                />
              </FormGroup>
            </Col>
            <Col lg="6" md="6" sm="12">
              <FormGroup>
                <Label for="basicInput">License Number</Label>
                <Input
                  type="text"
                  id="basicInput"
                  value={bank_name}
                  onChange={(e) => {
                    this.setState({
                      bank_name: e.target.value,
                    });
                  }}
                />
              </FormGroup>
            </Col>
            <Col lg="12" md="12" sm="12">
              <FormGroup>
                <Label for="basicInput">Address</Label>
                <Input
                  type="text"
                  id="basicInput"
                  value={branch_name}
                  onChange={(e) => {
                    this.setState({
                      branch_name: e.target.value,
                    });
                  }}
                />
              </FormGroup>
            </Col>
            
              </Row>
            
            </Col>
            
            
          </Row>
        </CardBody>
      </Card>
   
          <Card>
        <CardHeader>
          <div className="title">
            <CardTitle>Contact</CardTitle>
          </div>
        </CardHeader>
        <CardBody>
          <Row>
            <Col lg="6" md="6" sm="12">
              <FormGroup>
                <Label for="basicInput">Country</Label>
                <Input
                  type="text"
                  id="basicInput"
                  value={account_holder_name}
                  onChange={(e) => {
                    this.setState({
                      account_holder_name: e.target.value,
                    });
                  }}
                />
              </FormGroup>
            </Col>
            <Col lg="6" md="6" sm="12">
              <FormGroup>
                <Label for="basicInput">Address</Label>
                <Input
                  type="text"
                  id="basicInput"
                  value={bank_name}
                  onChange={(e) => {
                    this.setState({
                      bank_name: e.target.value,
                    });
                  }}
                />
              </FormGroup>
            </Col>
            <Col lg="6" md="6" sm="12">
              <FormGroup>
                <Label for="basicInput">City</Label>
                <Input
                  type="text"
                  id="basicInput"
                  value={branch_name}
                  onChange={(e) => {
                    this.setState({
                      branch_name: e.target.value,
                    });
                  }}
                />
              </FormGroup>
            </Col>
            <Col lg="6" md="6" sm="12">
              <FormGroup>
                <Label for="basicInput">Phone</Label>
                <Input
                  type="text"
                  id="basicInput"
                  value={account_number}
                  onChange={(e) => {
                    this.setState({
                      account_number: e.target.value,
                    });
                  }}
                />
              </FormGroup>
            </Col>
            <Col lg="6" md="6" sm="12">
              <FormGroup>
                <Label for="basicInput">Email</Label>
                <Input
                  type="text"
                  id="basicInput"
                  value={ifsc}
                  onChange={(e) => {
                    this.setState({
                      ifsc: e.target.value,
                    });
                  }}
                />
              </FormGroup>
            </Col>
            <Col lg="6" md="6" sm="12">
              <FormGroup>
                <Label for="basicInput">Pincode</Label>
                <Input
                  type="text"
                  id="basicInput"
                  value={pancard}
                  onChange={(e) => {
                    this.setState({
                      pancard: e.target.value,
                    });
                  }}
                />
              </FormGroup>
            </Col>
            <Col lg="6" md="6" sm="12">
              <FormGroup>
                {/* <Label for="basicInput">Home Delivery</Label>
                <Input
                  type="radio"
                  id="basicInput"
                  value={gstin}
                  onChange={(e) => {
                    this.setState({
                      gstin: e.target.value,
                    });
                  }}
                /> */}
              </FormGroup>
            </Col>
            <Col lg="6" md="6" sm="12">
              <FormGroup>
              <label
                  className="btn btn-flat-primary"
                  htmlFor="update-image"
                  color="primary">
                  Upload Certificate
                  <input
                    type="file"
                    id="update-image"
                    hidden
                    onChange={e =>
                      this.setState({
                        img: URL.createObjectURL(e.target.files[0])
                      })
                    }
                  />
                </label>
              </FormGroup>
            </Col>
            <Col lg="12" md="12" sm="12" className="account-footer">
              <FormGroup>
                <div className="button-footer">
                  <button
                    className="btn reset-all"
                    onClick={() => {
                      console.log('here');
                    }}
                    style={{ fontWeight: '600' }}
                  >
                    Reset All
                  </button>
                  <button
                    className="btn btn-primary float-right "
                    onClick={() => this.handleSubmit()}
                    style={{ fontWeight: '600' }}
                  >
                    Save Changes
                  </button>
                </div>
              </FormGroup>
            </Col>
           
          </Row>
        </CardBody>
      </Card>
      
      </React.Fragment>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    bank_details: state.bank.bank_details,
  };
};

export default connect(mapStateToProps, {
  updateProfile
})(ProfileEdit);
