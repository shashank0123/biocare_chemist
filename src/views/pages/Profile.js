import React, { Component } from 'react';
import {
  Card,
  CardBody,
  CardHeader,
  Media,
  CardTitle,
  CustomInput,
} from 'reactstrap';
import '../../assets/scss/pages/users.scss';
import userImg from '../../assets/img/portrait/small/avatar-s-18.jpg';
import { Edit3 } from 'react-feather';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

class Profile extends Component {
  render() {
    return (
      <Card>
        <CardHeader>
          <div className="title">
            <CardTitle>Info</CardTitle>
          </div>
          <Link to="user-profile/edit" className="edit-profile">
            <Edit3 className="cursor-pointer" size={20} />
          </Link>
        </CardHeader>
        <CardBody>
          <div className="users-page-view-table">
            <div className="d-flex user-info">
              <div className="user-info-title font-weight-bold">
                <Media className="mt-md-1 mt-0" left>
                  <Media
                    className="rounded mr-2"
                    object
                    src={this.props.user_details.cover_photo}
                    alt="chemist Image"
                    height="100"
                    width="100"
                  />
                </Media>
              </div>
            </div>

            <div className="d-flex user-info">
              <div className="user-info-title font-weight-bold">
                <h4>{this.props.user_details.chemist_name}</h4>
              </div>
            </div>
            <div className="d-flex user-info">
              <div className="user-info-title font-weight-bold">Address:</div>
              <div>{this.props.user_details.address}</div>
            </div>
            <div className="d-flex user-info">
              <div className="user-info-title font-weight-bold">Email ID:</div>
              <div>{this.props.user_details.email}</div>
            </div>
            <div className="d-flex user-info">
              <div className="user-info-title font-weight-bold">Contact:</div>
              <div>{this.props.user_details.phone}</div>
            </div>
            <div className="d-flex user-info">
              <div className="user-info-title font-weight-bold">
                Subscription Last Date:
              </div>
              <div>{this.props.user_details.subscribe_date}</div>
            </div>
            <div className="d-flex user-info">
              <div className="user-info-title font-weight-bold">
                License Number
              </div>
              <div>{this.props.user_details.licence_number}</div>
            </div>
            {/* <div className="d-flex user-info">
              <div className="user-info-title font-weight-bold">Home Delivery</div>
              <div>
              <CustomInput
            inline
            type="checkbox"
            id="exampleCustomCheckbox"
            label="Active"
            defaultChecked
          />
          <CustomInput
            inline
            type="checkbox"
            id="exampleCustomCheckbox"
            label="Active"
            defaultChecked
          />
              </div>
            </div> */}
          </div>
        </CardBody>
      </Card>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    user_details: state.auth.login.user_details,
  };
};

export default connect(mapStateToProps, {})(Profile);
