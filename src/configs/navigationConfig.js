import React from "react"
import * as Icon from "react-feather"
const navigationConfig = [
  {
    id: "dashboard",
    title: "Dashboard",
    type: "item",
    icon: <Icon.Home size={20} />,
    permissions: ["admin", "editor"],
    navLink: "/",
  },
  {
    id: "profile",
    title: "Profile",
    type: "item",
    icon: <Icon.File size={20} />,
    permissions: ["admin", "editor"],
    navLink: "/user-profile",
  },
  {
    id: "viewOrder",
    title: "View Order",
    type: "item",
    icon: <Icon.File size={20} />,
    permissions: ["admin", "editor"],
    navLink: "/view-order",
  },
  {
    id: "completeOrders",
    title: "Complete Orders",
    type: "item",
    icon: <Icon.File size={20} />,
    permissions: ["admin", "editor"],
    navLink: "/completed-order",
  },
  {
    id: "history",
    title: "History",
    type: "item",
    icon: <Icon.File size={20} />,
    permissions: ["admin", "editor"],
    navLink: "/history",
  },
  {
    id: "cancelledOrders",
    title: "Cancelled Order",
    type: "item",
    icon: <Icon.File size={20} />,
    permissions: ["admin", "editor"],
    navLink: "/cancelled-order",
  },
  {
    id: "bankDetails",
    title: "Bank Details",
    type: "item",
    icon: <Icon.File size={20} />,
    permissions: ["admin", "editor"],
    navLink: "/bank-details"
  }        
]

export default navigationConfig
