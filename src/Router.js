import React, { Suspense, lazy } from 'react';
import { Router, Switch, Route, Redirect } from 'react-router-dom';
import { history } from './history';
import { connect } from 'react-redux';
import Spinner from './components/@vuexy/spinner/Loading-spinner';
import { ContextLayout } from './utility/context/Layout';

// Route-based code splitting
const Dashboard = lazy(() => import('./views/pages/dashboard/Dashboard'));
const Profile = lazy(() => import('./views/pages/Profile'));
const ViewOrder = lazy(() => import('./views/pages/ViewOrder'));
const CompleteOrder = lazy(() => import('./views/pages/CompleteOrder'));
const History = lazy(() => import('./views/pages/History'));
const CancelledOrder = lazy(() => import('./views/pages/CancelledOrder'));
const BankDetails = lazy(() => import('./views/pages/BankDetails'));
const BankDetailsEdit = lazy(() => import('./views/pages/BankDetailsEdit'));
const ProfileEdit = lazy(()=>import('./views/pages/ProfileEdit'))
const login = lazy(() => import('./views/pages/authentication/login/Login'));

// Set Layout and Component Using App Route
const RouteConfig = ({
  component: Component,
  fullLayout,
  permission,
  user,
  ...rest
}) => (
  <Route
    {...rest}
    render={(props) => {
      return (
        <React.Fragment>
          {user.loading?<Spinner/>:
          user.isAuthenticated || fullLayout ? (
            <ContextLayout.Consumer>
              {(context) => {
                let LayoutTag =
                  fullLayout === true
                    ? context.fullLayout
                    : context.state.activeLayout === 'horizontal'
                    ? context.horizontalLayout
                    : context.VerticalLayout;
                return (
                  <React.Fragment>
                    <LayoutTag {...props} permission={props.user}>
                      <Suspense fallback={<Spinner />}>
                        <Component {...props} />
                      </Suspense>
                    </LayoutTag>
                  </React.Fragment>
                );
              }}
            </ContextLayout.Consumer>
          ) : (
            <React.Fragment>
              <Suspense fallback={<Spinner />}>
                <Redirect to="/login" />
                <Component {...props} />
              </Suspense>
            </React.Fragment>
          )}
        </React.Fragment>
      );
    }}
  />
);
const mapStateToProps = (state) => {
  return {
    user: state.auth.login,
  };
};

const AppRoute = connect(mapStateToProps)(RouteConfig);

class AppRouter extends React.Component {
  render() {
    return (
      // Set the directory path if you are deploying in sub-folder
      <Router history={history}>
        <Switch>
          <AppRoute exact path="/" component={Dashboard} />
          <AppRoute exact path="/user-profile" component={Profile} />
          <AppRoute exact path="/view-order" component={ViewOrder} />
          <AppRoute exact path="/completed-order" component={CompleteOrder} />
          <AppRoute exact path="/history" component={History} />
          <AppRoute exact path="/cancelled-order" component={CancelledOrder} />
          <AppRoute exact path="/bank-details" component={BankDetails} />
          <AppRoute
            exact
            path="/bank-details/edit"
            component={BankDetailsEdit}
          />
          <AppRoute
            exact
            path="/user-profile/edit"
            component={ProfileEdit}
          />
          <AppRoute path="/login" component={login} fullLayout />
        </Switch>
      </Router>
    );
  }
}

export default AppRouter;
