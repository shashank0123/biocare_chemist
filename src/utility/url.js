const urlObj = {
    login_url: 'http://api.bionische.com/api/chemist/login',
    validate_token:"http://api.bionische.com/api/validate-token",
    get_profile:'http://api.bionische.com/api/chemists',
    banks:'http://api.bionische.com/api/chemist/banks',//GET POST DELETE PUT->update
    completed_order:'http://api.bionische.com/api/chemist/order/completed',
    cancelled_order:'http://api.bionische.com/api/chemist/order/canceled',
    recent_order:'http://api.bionische.com/api/chemist/order/recently',
    appointment:'http://api.bionische.com/api/chemist/appointments',
    bank_detail:'http://api.bionische.com/api/chemist/banks',
    dashboard:'http://api.bionische.com/api/chemist/dashboard'

    

  };
  
  module.exports = urlObj;