import React, { Component } from "react"
import { NavLink } from "react-router-dom"
import { Disc, X, Circle,List } from "react-feather"
import classnames from "classnames"
import logo from "../../../../assets/img/logo/logo.PNG"
class SidebarHeader extends Component {
  render() {
    let {
      toggleSidebarMenu,
      activeTheme,
      collapsed,
      toggle,
      sidebarVisibility,
      menuShadow
    } = this.props
    return (
      <div className="navbar-header">
        <ul className="nav navbar-nav flex-row">
          <li className="nav-item mr-auto">
            <NavLink to="/" className="navbar-brand">
              <div className="brand-logo" >
                <img className="logo-img" src={logo} />
                </div>
              <h2 className="brand-text mb-0">IOCARE</h2>
              
            </NavLink>
            <p className="panel">Chemist Panel</p>
          </li>
          <li className="nav-item nav-toggle">
            <div className="nav-link modern-nav-toggle">
              {collapsed === false ? (
                <List
                  onClick={() => {
                    toggleSidebarMenu(true)
                    toggle()
                  }}
                  className={classnames(
                    "toggle-icon icon-x d-none d-xl-block font-medium-4",
                  )}
                  style={{
                    color:"white"
                  }}
                  size={20}
                  data-tour="toggle-icon"
                />
              ) : (
                <List
                  onClick={() => {
                    toggleSidebarMenu(false)
                    toggle()
                  }}
                  className={classnames(
                    "toggle-icon icon-x d-none d-xl-block font-medium-4",
                  )}
                  style={{
                    color:"white"
                  }}
                  size={20}
                />
              )}
              <X
                onClick={sidebarVisibility}
                className={classnames(
                  "toggle-icon icon-x d-block d-xl-none font-medium-4",
                )}
                style={{
                  color:"white"
                }}
                size={20}
              />
            </div>
          </li>
        </ul>
        <div
          className={classnames("shadow-bottom", {
            "d-none": menuShadow === false
          })}
        />
      </div>
    )
  }
}

export default SidebarHeader
