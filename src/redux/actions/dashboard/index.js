import axios from 'axios';
import url from '../../../utility/url';
export const getDashboard = () =>{
  return async (dispatch, getState) => {
    await axios
      .get(url.dashboard)
      .then((response) => {
        console.log(response)
        dispatch({
          type:"GET_DASHBOARD_DETAILS",
          revenue:response.data.data.first,
          summary:response.data.data.second,
          total_order:response.data.data.third
        })
      })
      .catch((error) => {
        console.log(error);
      });
  };
}
