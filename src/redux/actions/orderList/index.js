import axios from 'axios';
import url from '../../../utility/url';
export const completedOrder = () => {
  return async (dispatch, getState) => {
    await axios
      .get(url.completed_order)
      .then((response) => {
        dispatch({
          type: 'COMPLETED_ORDER',
          payload: response.data.data,
        });
        console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const cancelledOder = () => {
  return async (dispatch, getState) => {
    await axios
      .get(url.cancelled_order)
      .then((response) => {
        dispatch({
          type: 'CANCELLED_ORDER',
          payload: response.data.data,
        });
        console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};

export const recentOrder = () => {
  return async (dispatch, getState) => {
    await axios
      .get(url.recent_order)
      .then((response) => {

        dispatch({
          type: 'RECENT_ORDER',
          payload: response.data.data,
        });
        console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};

export const appointment = (value) => {
  return async (dispatch, getState) => {
    await axios
      .get(url.appointment)
      .then((response) => {
        dispatch({
          type: 'APPOINTMENT',
          payload: response.data.data,
        });
        console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
