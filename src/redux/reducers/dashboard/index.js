const initialState = {
  revenue: '',
  summary:'',
  total_order:'',
  total_order_sales:[],
  revenue_expenses:[],
  revenue_income:[],
  revenue_months:[]
};

export const bank = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_DASHBOARD_DETAILS': {
      return { ...state, revenue: action.revenue,revenue_expenses:action.revenue.expenses,revenue_income:action.revenue.income,revenue_months:action.revenue.months,summary:action.summary,total_order:action.total_order,total_order_sales:action.total_order.sales };
    }
    default: {
      return state;
    }
  }
};
export default bank;
