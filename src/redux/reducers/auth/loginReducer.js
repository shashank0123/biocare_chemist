export const login = (state = { userRole: "admin",isAuthenticated:false,loading:true,user_details:'' }, action) => {
  switch (action.type) {
    case "LOGIN_WITH_EMAIL": {
      return { ...state, token: action.payload,isAuthenticated:true }
    }
    case "LOGIN_VALIDATE": {
      return { ...state,token:localStorage.getItem('token'),isAuthenticated:true }
    }
    case "LOGIN_WITH_ERROR": {
      return { ...state, error: action.payload }
    }
    case "GET_PROFILE": {
      return { ...state, user_details: action.payload }
    }
    case "LOADING_FALSE":{
      return{...state,loading:false}
    }
    default: {
      return state
    }
  }
}
