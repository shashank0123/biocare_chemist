import { combineReducers } from "redux"
import customizer from "./customizer/"
import auth from "./auth/"
import navbar from "./navbar/Index"
import orderList from "./oder-list/index"
import bank from "./bank"
import dashboard from "./dashboard"

const rootReducer = combineReducers({
  customizer: customizer,
  auth: auth,
  navbar: navbar,
  orderList:orderList,
  bank:bank,
  dashboard:dashboard
})

export default rootReducer
